package com.qiscus.qiscuscall

import android.app.Application
import android.content.Context
import com.nexmo.client.NexmoCall
import com.nexmo.client.NexmoClient
import com.nexmo.client.NexmoUser

var currentCall: NexmoCall? = null
var currentUser: NexmoUser? = null

class QiscusCall {
    companion object {
        lateinit var nexmoClient: NexmoClient
        var userNameUser1 = "setiawan"
        var userNameUser2 = "yoga"
        var jwtUser1 =
            "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NzY1NTQ1OTMsImp0aSI6IjQ4ZTAxODUwLTIwODAtMTFlYS04M2Q2LTIxY2RmNmRiMGFhYSIsInN1YiI6InNldGlhd2FuIiwiZXhwIjoxNTc2NjQwOTkzLCJhY2wiOnsicGF0aHMiOnsiL3YxL3Nlc3Npb25zLyoqIjp7fSwiL3YxL3VzZXJzLyoqIjp7fSwiL3YxL2NvbnZlcnNhdGlvbnMvKioiOnt9fX0sImFwcGxpY2F0aW9uX2lkIjoiNmU1MDE0ZjYtYWFlMS00M2E2LThiNDEtZTcwZWIxYTZhNmY0In0.S6wh9erWGsHC-KOUbGDzYIpX3B3euVMveVTM4u7HTRwmxJziWiA-bYWJcSGBa48YJtnQMzvkmHyNJyYMGWkBFPQYysgcjjMmXZlZKp4R4WNo17I8c2vq36w_LCSx6PUFjOx5To5Mt-lh8JNdiVtR1lnHqo6qYFEsj3o09H1vHGR9uI4HHG_HtFWAv-gviFdSbkKLmHQffIsAuIwNI-tfyLm4SpMkPiHHDAkNFXyap4_rwO6PR2TQq-mzX9-1A-URoiIEalZk19-auPGwy3jOcwww1S85EbCgyalzrcDyDsWEWSN8mR3o1SLT9ZckssxaV09hhWmpsSf8TqTv8_Q5AQ"
        var jwtUser2 =
            "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NzY1NTQ1OTIsImp0aSI6IjQ3ZWViMzcwLTIwODAtMTFlYS04M2ZmLTJmZThhYjI4YzI4ZCIsInN1YiI6InlvZ2EiLCJleHAiOjE1NzY2NDA5OTEsImFjbCI6eyJwYXRocyI6eyIvdjEvc2Vzc2lvbnMvKioiOnt9LCIvdjEvdXNlcnMvKioiOnt9LCIvdjEvY29udmVyc2F0aW9ucy8qKiI6e319fSwiYXBwbGljYXRpb25faWQiOiI2ZTUwMTRmNi1hYWUxLTQzYTYtOGI0MS1lNzBlYjFhNmE2ZjQifQ.m5szXwesJOzsRC8hb-LyhZFhs1NVYgJCAvYec6Bj-Gel0ICTful6ioU55Rk-SKSHxfXNrmWdENcY6KI6wEnISSiMSfTIC6ay1nAkKOb2TfiC-2ST-TEgsU7CTecsxZudi25ZgtUurj_0PJoMDtcb-_X5YGyMd2ka3DWeiYU9N6Gdkh_SGRecEC5w5wl6ZTrCt2vq00JGxgZOu6VXSqj4mBKFiNgZ9pBEFUW-cLqaVBvEz_PzDtSvKeTm9Z9AaEFwM0BziVxd03L3v95eoC5mdv7IDtMmk7phtEfypuREbiIKVlJBGN0YHKFalPisX_nCNGl0vtivSxbCsf6C8SwydQ"
        var TAG = "LOG_APP"
        @JvmStatic
        fun init(application: Application, context: Context) {
            nexmoClient = NexmoClient.Builder().build(context)
        }
    }

}