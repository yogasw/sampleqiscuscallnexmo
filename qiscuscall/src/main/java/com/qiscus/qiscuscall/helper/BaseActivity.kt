package com.qiscus.qiscuscall.helper

import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.nexmo.client.request_listener.NexmoApiError
import com.qiscus.qiscuscall.QiscusCall

abstract class BaseActivity : AppCompatActivity() {
    private val CALL_PERMISSIONS_REQ = 121
    private val TAG = QiscusCall.TAG

    companion object {
        private val callsPermissions = arrayOf(
            android.Manifest.permission.READ_PHONE_STATE,
            android.Manifest.permission.RECORD_AUDIO,
            android.Manifest.permission.PROCESS_OUTGOING_CALLS
        )
    }

    override fun onStart() {
        super.onStart()
        if (doesNeedCallPermissions()) {
            requestCallPermissions()
        }
    }

    fun requestCallPermissions() {
        ActivityCompat.requestPermissions(this, callsPermissions, CALL_PERMISSIONS_REQ)
    }

    fun doesNeedCallPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (perm in callsPermissions) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        perm
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return true
                }
            }
        }
        return false
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode != CALL_PERMISSIONS_REQ) {
            requestCallPermissions()
        }
        val audioManager: AudioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        audioManager.mode = AudioManager.MODE_IN_CALL
        audioManager.isSpeakerphoneOn = true

        fun notifyError(nexmoApiError: String) {
            Log.e(TAG, nexmoApiError)
            Toast.makeText(this, TAG, Toast.LENGTH_LONG).show()
        }
    }
}