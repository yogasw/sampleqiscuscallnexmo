package com.qiscus.qiscuscall.ui.qiscusCall

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.nexmo.client.NexmoCall
import com.nexmo.client.NexmoClient
import com.nexmo.client.NexmoUser
import com.nexmo.client.request_listener.NexmoApiError
import com.qiscus.qiscuscall.QiscusCall
import com.qiscus.qiscuscall.R
import com.qiscus.qiscuscall.currentCall
import com.qiscus.qiscuscall.currentUser
import com.qiscus.qiscuscall.helper.BaseActivity
import com.qiscus.qiscuscall.ui.Call.CallActivity
import com.qiscus.qiscuscall.ui.Calling.CallingActivity
import kotlinx.android.synthetic.main.activity_qiscus_call.*


class QiscusCallActivity : QiscusCallView, BaseActivity() {
    private lateinit var presenter: QiscusCallPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qiscus_call)

        presenter = QiscusCallPresenter(this, QiscusCall.nexmoClient)

        btn_login1.setOnClickListener {
            presenter.login(QiscusCall.jwtUser1)
        }

        btn_login2.setOnClickListener {
            presenter.login(QiscusCall.jwtUser2)
        }

        btn_call.setOnClickListener {
            val username =
                if (currentUser?.name == QiscusCall.userNameUser1) QiscusCall.userNameUser2 else QiscusCall.userNameUser1
            presenter.call(username)
        }
    }

    override fun loginSuccess(p0: NexmoUser?) {
        currentUser = p0
        runOnUiThread {
            tv_user.text = p0?.name
            Toast.makeText(this, "Login success user : ${p0?.name}", Toast.LENGTH_SHORT).show()
            presenter.incomingCall()
        }
    }

    override fun lonError(p0: NexmoApiError) {
        runOnUiThread {
            Toast.makeText(this, "Error : ${p0.message}", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCallSuccess(p0: NexmoCall?) {
        currentCall = p0
        runOnUiThread {
            startActivity(Intent(this@QiscusCallActivity, CallActivity::class.java))
        }
    }

    override fun onCallError(p0: NexmoApiError) {
        lonError(p0)
    }

    override fun onIncomingCall(nexmoCall: NexmoCall?) {
        currentCall = nexmoCall
        /*  val audioManager: AudioManager = getSystemService(AUDIO_SERVICE) as AudioManager
          audioManager.mode = AudioManager.MODE_IN_CALL
          audioManager.isSpeakerphoneOn = true*/

        startActivity(Intent(this, CallingActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        NexmoClient.get().removeIncomingCallListeners()
    }
}
