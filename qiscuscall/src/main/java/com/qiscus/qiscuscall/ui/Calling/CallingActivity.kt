package com.qiscus.qiscuscall.ui.Calling

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.nexmo.client.NexmoCall
import com.nexmo.client.NexmoCallEventListener
import com.nexmo.client.request_listener.NexmoApiError
import com.nexmo.client.request_listener.NexmoRequestListener
import com.qiscus.qiscuscall.helper.FinishOnCallEnd
import com.qiscus.qiscuscall.R
import com.qiscus.qiscuscall.currentCall
import com.qiscus.qiscuscall.helper.BaseActivity
import com.qiscus.qiscuscall.ui.Call.CallActivity
import com.qiscus.qiscuscall.ui.qiscusCall.QiscusCallActivity
import kotlinx.android.synthetic.main.activity_calling.*


class CallingActivity : BaseActivity(), CallingView {
    //lateinit var presenter: CallingPresenter
    lateinit var mp :MediaPlayer

    lateinit var callEventListener: NexmoCallEventListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calling)

        callEventListener =  FinishOnCallEnd(this)

        mp =  MediaPlayer.create(this,R.raw.ringing)
        playRinging()

        currentCall?.addCallEventListener(callEventListener)

        currentCall?.let {
            tv_name.text = it.callMembers?.first()?.user?.name
        }
        btn_mute.setOnClickListener {
            //presenter.answer()
            currentCall?.answer(object : NexmoRequestListener<NexmoCall> {
                override fun onError(nexmoApiError: NexmoApiError) {
                   // notifyError(nexmoApiError)
                    logAndShow(nexmoApiError.message)
                }

                override fun onSuccess(call: NexmoCall?) {
                    startActivity(Intent(this@CallingActivity, CallActivity::class.java))
                    finish()
                }
            })
        }
        btn_speaker.setOnClickListener {
           // presenter.endCall()
            currentCall?.hangup(object : NexmoRequestListener<NexmoCall> {
                override fun onError(nexmoApiError: NexmoApiError) {
                    logAndShow(nexmoApiError.message)
                }

                override fun onSuccess(call: NexmoCall?) {
                    startActivity(Intent(this@CallingActivity, QiscusCallActivity::class.java))
                    finish()
                }
            })

            finish()
        }

        //presenter = CallingPresenter(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mp.stop()
        currentCall?.removeCallEventListener(callEventListener)
    }

    override fun logAndShow(s: String) {
        Log.d("LOG_APP", s)
        runOnUiThread {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCallConnected() {
        val intent = Intent(this, CallActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun endCall() {
        mp.stop()
        currentCall?.removeCallEventListener(callEventListener)
        currentCall = null
    }

    override fun connecting(s: String) {
        runOnUiThread {
            tv_status.text = s
        }
    }

    private fun playRinging() {
        mp.start()
    }
}
