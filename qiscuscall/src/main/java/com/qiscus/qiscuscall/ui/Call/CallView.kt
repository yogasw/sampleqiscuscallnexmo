package com.qiscus.qiscuscall.ui.Call

import com.nexmo.client.NexmoCall
import com.nexmo.client.request_listener.NexmoApiError

interface CallView {
    fun showError(nexmoApiError: NexmoApiError)
    fun endCallSuccess(call: NexmoCall?)

}
