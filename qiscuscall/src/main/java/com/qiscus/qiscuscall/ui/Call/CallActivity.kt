package com.qiscus.qiscuscall.ui.Call

import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import com.bumptech.glide.Glide
import com.nexmo.client.NexmoCall
import com.nexmo.client.NexmoCallEventListener
import com.nexmo.client.NexmoCallMember
import com.nexmo.client.NexmoCallMemberStatus
import com.nexmo.client.request_listener.NexmoApiError
import com.nexmo.client.request_listener.NexmoRequestListener
import com.qiscus.qiscuscall.R
import com.qiscus.qiscuscall.currentCall
import com.qiscus.qiscuscall.helper.BaseActivity
import com.qiscus.qiscuscall.helper.FinishOnCallEnd
import com.qiscus.qiscuscall.helper.FinishOnCallEnd.Callback
import com.qiscus.qiscuscall.ui.qiscusCall.QiscusCallActivity
import kotlinx.android.synthetic.main.activity_call.*

class CallActivity : BaseActivity(), CallView {
    lateinit var mp: MediaPlayer

    private lateinit var callEventListener: NexmoCallEventListener
    //lateinit var presenter: CallPresenter
    private lateinit var audioManager: AudioManager
    private var mute = false
    private var speaker = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call)
        callEventListener = FinishOnCallEnd(this)
        mp = MediaPlayer.create(this, R.raw.ringing)
        playAudio()

        (callEventListener as FinishOnCallEnd).testing = object : Callback {
            override fun testing() {
                tv_status.text = "Halo"
            }

            override fun onMemberStatusUpdated(
                nexmoCallStatus: NexmoCallMemberStatus,
                callMember: NexmoCallMember
            ) {
                tv_username.text = callMember.user.name
            }
        }
        currentCall?.addCallEventListener(callEventListener)

        audioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        audioManager.mode = AudioManager.MODE_IN_CALL
        audioManager.isSpeakerphoneOn = true
        // presenter = CallPresenter(this)

        btn_end_call.setOnClickListener {
            endCall()
        }
        btn_speaker.setOnClickListener {
            speaker()
        }

        btn_mute.setOnClickListener {
            mute()
        }
    }

    private fun endCall() {

        currentCall?.hangup(object : NexmoRequestListener<NexmoCall> {
            override fun onSuccess(call: NexmoCall?) {
                currentCall = null
                finish()
            }

            override fun onError(nexmoApiError: NexmoApiError) {
                showError(nexmoApiError)
            }
        })

    }

    private fun playAudio() {
        //mp.start()
    }

    private fun mute() {
        mute = !mute
        audioManager.isMicrophoneMute = mute
        setBtnMute()
    }

    private fun speaker() {
        speaker = !speaker
        audioManager.isSpeakerphoneOn = speaker
        setIconSpeaker()
    }

    private fun setIconSpeaker() {
        val icon = if (speaker) R.drawable.ic_speaker_on else R.drawable.ic_speaker_off
        Glide.with(this)
            .load(icon)
            .into(btn_speaker)
    }

    override fun showError(nexmoApiError: NexmoApiError) {
        // showError(nexmoApiError)
    }

    override fun endCallSuccess(call: NexmoCall?) {
        finish()
        currentCall?.removeCallEventListener(callEventListener)
    }

    private fun setBtnMute() {
        val icon = if (mute) R.drawable.ic_microphone_off else R.drawable.ic_microphone_on
        Glide.with(this)
            .load(icon)
            .into(btn_mute)
    }

    override fun onStart() {
        super.onStart()
        currentCall?.addCallEventListener(callEventListener)
    }

    override fun onStop() {
        currentCall?.removeCallEventListener(callEventListener)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        currentCall = null
    }
}
