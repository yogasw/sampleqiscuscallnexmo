package com.qiscus.qiscuscall.ui.Calling

interface CallingView {
    fun logAndShow(s: String)
    fun onCallConnected()
    fun endCall()
    fun connecting(s: String)
}
