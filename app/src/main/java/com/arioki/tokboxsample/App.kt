package com.arioki.tokboxsample

import android.app.Application
import com.qiscus.qiscuscall.QiscusCall

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        QiscusCall.init(this, this)
    }
}